# Doodle Code challenge #

Repository contains a solution for [coding challenge](https://github.com/DoodleScheduling/hiring-challenges/tree/master/backend-engineer) for a back-end position. 

# Challenge analysis #
1. Analysis of provided assets helped determine domain model structure, primary and foreign, relation types between nested objects..
2. Requirements for API functionality helped define endpoints and required db queries

# Technology stack #

* PostgresDb
* Java with Spring Boot
* Spring Data
* Docker Compose

# Desision points #

##### Database #####
Based on pretty loose API requirements and data set size, I've decided to go with postgres db. It's well known solution that has 
been around for ages having vast community support. It has support for json processing, inverted index and full text search (not all languages are supported)
    
We could also go with nosql db, or even a mix of sql and nosql. Having more bigger picture around needed endpoints, 
load distribution between them and other operations that should be performed against persisted data we could refine which database could be more performant.

##### Spring framework #####
Due to all reasons that are making developers life easier, I've decided to go with Spring Boot, Spring MVC and 
Data module. It helped a lot with boilerplate code, configurations and integrations with db.

Spring Data module is automating repository implementations, object mappings... but it might introduce weird behaviors under the hood, problems that could be hard to notice, so needs to be taken with caution.  


##### Points for improvement #####

* One-to-Many mapping could be made more performant using bidirectional relation between Poll and Participant entity
* Improve search by poll title so it's case insensitive
* Go with a single endpoint that has a needed requirements (filtering by date, user, title)
 ...