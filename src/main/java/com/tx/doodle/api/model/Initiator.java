package com.tx.doodle.api.model;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "initiator",
        uniqueConstraints = @UniqueConstraint(name = "uk_email", columnNames = {"email"}))
@Data
@NoArgsConstructor
public class Initiator {
    @Id
    @GeneratedValue
    private long id;
    private String name;
    private String email;
    private boolean notify;
}
