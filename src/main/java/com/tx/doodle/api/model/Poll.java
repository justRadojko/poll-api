package com.tx.doodle.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tx.doodle.api.util.ByteListToCommaSeparatedConverter;
import com.tx.doodle.api.util.JsonArrayConverter;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Entity
@Data
@NoArgsConstructor
public class Poll {

    @Id
    private String id;
    private String adminKey;
    private Timestamp latestChange;
    private Timestamp initiated;
    private PoolType type;
    private boolean hidden;

    @Enumerated(value = EnumType.STRING)
    private PreferencesType preferencesType;

    private State state;
    private Locale locale;
    private String title;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "initiator_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Initiator initiator;

    @Column(columnDefinition = "text")
    @Convert(converter = JsonArrayConverter.class)
    private List<Map<String, Object>> options;

    private String optionsHash;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "poll_id")
    private List<Participant> participants;

    private int participantsCount;

    @Convert(converter = ByteListToCommaSeparatedConverter.class)
    private List<String> invitees;

    private int inviteesCount;
    private Device device;
    private Levels levels;


    public enum PoolType {
        DATE, TEXT
    }

    public enum PreferencesType {
        YESNO, YESNOIFNEEDBE
    }

    public enum Levels {
        YESNO, YESNOIFNEEDBE
    }

    public enum State {
        OPEN
    }

    public enum Device {
        WEB
    }
}
