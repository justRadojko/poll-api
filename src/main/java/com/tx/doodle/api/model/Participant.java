package com.tx.doodle.api.model;

import com.tx.doodle.api.util.ByteListToCommaSeparatedConverter;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "participant")
@Data
@NoArgsConstructor
public class Participant {

    @Id
    private String id;
    private String name;

    @Convert(converter = ByteListToCommaSeparatedConverter.class)
    private List<Byte> preferences;

}
