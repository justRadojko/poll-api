package com.tx.doodle.api.dao;

import com.tx.doodle.api.model.Initiator;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<Initiator, Long> {
    Optional<Initiator> findFirstByEmail(String email);
}
