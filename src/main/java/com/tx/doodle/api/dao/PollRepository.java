package com.tx.doodle.api.dao;

import com.tx.doodle.api.model.Initiator;
import com.tx.doodle.api.model.Poll;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface PollRepository extends JpaRepository<Poll, String> {

    @Query("SELECT DISTINCT pl FROM Poll pl LEFT JOIN FETCH pl.participants WHERE pl.title LIKE %:title%")
    List<Poll> searchPollByTitle(@Param("title") String title, Pageable pageable);

    @Query("SELECT DISTINCT pl FROM Poll pl LEFT JOIN FETCH pl.participants " +
            "WHERE ( :initiator IS NULL OR pl.initiator = :initiator)" +
            "AND ( CAST(:fromDate AS date) IS NULL OR pl.initiated > :fromDate)" +
            "ORDER BY pl.initiated DESC")
    List<Poll> filterPolls(@Param("initiator") Initiator initiator, @Param("fromDate") Timestamp fromDate, Pageable pageable);
}
