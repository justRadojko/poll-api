package com.tx.doodle.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoodlesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoodlesApplication.class, args);
	}

}
