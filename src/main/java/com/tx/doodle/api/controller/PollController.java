package com.tx.doodle.api.controller;

import com.tx.doodle.api.model.Poll;
import com.tx.doodle.api.service.PollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;

import static com.google.common.base.Strings.isNullOrEmpty;


@Controller
@RequestMapping(value = "/polls")
public class PollController {

    private static final String DEFAULT_POLL_PAGE_SIZE = "5";
    private static final int MAX_POLL_PAGE_SIZE = 100;

    PollService pollService;

    @Autowired
    public PollController(PollService pollService) {
        this.pollService = pollService;
    }

    @GetMapping("/filter")
    public ResponseEntity<List<Poll>> getPolls(@RequestParam(value = "email", required = false) String email,
                                               @RequestParam(value = "from", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
                                               @RequestParam(name = "page", defaultValue = "0") int page,
                                               @RequestParam(name = "size", defaultValue = DEFAULT_POLL_PAGE_SIZE) int size) {

        if (isNullOrEmpty(email) && fromDate == null) return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        try {
            Pageable pageable = PageRequest.of(page, Math.min(MAX_POLL_PAGE_SIZE, size), Sort.by("initiated"));

            return ResponseEntity.ok(pollService.filterPolls(email, fromDate, pageable));

        } catch (NoSuchElementException e) {

            // TODO Have an error response structure that could provide additional info
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping("/search")
    public ResponseEntity<List<Poll>> searchByTitle(@RequestParam(value = "title") String title,
                                                    @RequestParam(name = "page", defaultValue = "0") int page,
                                                    @RequestParam(name = "size", defaultValue = DEFAULT_POLL_PAGE_SIZE) int size) {

        Pageable pageable = PageRequest.of(page, Math.min(MAX_POLL_PAGE_SIZE, size), Sort.by("initiated"));

        if (title.length() >= 3) {
            return ResponseEntity.ok(pollService.searchPollsByTitle(title, pageable));
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}
