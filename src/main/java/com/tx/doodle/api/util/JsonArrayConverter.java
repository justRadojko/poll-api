package com.tx.doodle.api.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Slf4j
public class JsonArrayConverter implements AttributeConverter<List<Map<String, Object>>, String> {

    private ObjectMapper om = new ObjectMapper()
            .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

    @Override
    public String convertToDatabaseColumn(List<Map<String, Object>> jsonStruct) {
        String json = null;
        try {
            json = om.writeValueAsString(jsonStruct);
        } catch (final JsonProcessingException e) {
            log.error("Error occured converting json to db column", e);
        }

        return json;
    }

    @Override
    public List<Map<String, Object>> convertToEntityAttribute(String dbData) {
        List<Map<String, Object>> json = null;
        try {
            json = om.readValue(dbData, new TypeReference<>() {});
        } catch (final IOException e) {
            log.error("Error occured converting json to db column", e);
        }

        return json;
    }
}
