package com.tx.doodle.api.util;

import com.google.common.base.Strings;

import javax.persistence.AttributeConverter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class StringistToCommaSeparatedConverter implements AttributeConverter<List<String>, String> {

    private static final String DELIMITER = ",";

    @Override
    public String convertToDatabaseColumn(List<String> byteArray) {
        return byteArray.stream()
                .map(e -> e + "")
                .collect(Collectors.joining(DELIMITER));
    }

    @Override
    public List<String> convertToEntityAttribute(String dbData) {
        return Strings.isNullOrEmpty(dbData) ? Collections.emptyList()
                : Arrays.stream(dbData.split(DELIMITER))
                .collect(Collectors.toList());

    }
}
