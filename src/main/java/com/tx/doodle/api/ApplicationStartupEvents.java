package com.tx.doodle.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tx.doodle.api.model.Poll;
import com.tx.doodle.api.service.PollService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
public class ApplicationStartupEvents {

    private PollService pollService;

    @Autowired
    public ApplicationStartupEvents(PollService pollService) {
        this.pollService = pollService;
    }

    private static final String TEST_DATA_PATH = "test-data.json";
    ObjectMapper om = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
            .setSerializationInclusion(JsonInclude.Include.NON_NULL);

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) throws IOException {
        log.info("Start seeding test data...");

        InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream(TEST_DATA_PATH);
        List<Poll> pollList = Arrays.asList(om.readValue(input, Poll[].class));

        pollList.forEach(poll -> {
            try {
                pollService.importPoll(poll);
            } catch (Exception ex) {
                log.error("Error occurred trying to store a poll with id {}", poll.getId(), ex);
            }
        });

        log.info("Date seeding is completed");
    }
}
