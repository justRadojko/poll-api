package com.tx.doodle.api.service;

import com.tx.doodle.api.dao.PollRepository;
import com.tx.doodle.api.dao.UserRepository;
import com.tx.doodle.api.model.Initiator;
import com.tx.doodle.api.model.Poll;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static com.google.common.base.Strings.isNullOrEmpty;

@Slf4j
@Service
public class PollService {

    private PollRepository pollRepository;
    private UserRepository userRepository;

    @Autowired
    public PollService(PollRepository pollRepository, UserRepository userRepository){
        this.pollRepository = pollRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public void importPoll(Poll poll) {
        enhancePollWithInitiator(poll);

        pollRepository.save(poll);
    }

    // This method is used only for importing, since during import, we need to check if the initiator is present in
    // initiator table.
    // NOTE: initiator is not referenced through initiatorId, but whole Initiator object is present in poll variable passed
    // as an argument
    private void enhancePollWithInitiator(Poll poll) {
        if (poll.getInitiator() != null && !isNullOrEmpty(poll.getInitiator().getEmail())){

            Optional<Initiator> firstByEmail = userRepository.findFirstByEmail(poll.getInitiator().getEmail());

            poll.setInitiator(firstByEmail.orElseGet(() -> userRepository.save(poll.getInitiator())));

        } else {
            poll.setInitiator(null);
        }
    }

    public List<Poll> searchPollsByTitle(String title, Pageable pageable) {
        return pollRepository.searchPollByTitle(title, pageable);
    }

    public List<Poll> filterPolls(String userEmail, LocalDate fromDate, Pageable pageable) {
        Initiator initiator = null;

        if (!isNullOrEmpty(userEmail)) {
             initiator = userRepository.findFirstByEmail(userEmail)
                     .orElseThrow(() -> new NoSuchElementException(String.format("No initiator with email %s", userEmail)));
        }

        Timestamp fromDateTs = fromDate != null ? Timestamp.valueOf(fromDate.atStartOfDay()) : null;

        return pollRepository.filterPolls(initiator, fromDateTs, pageable);

    }
}
