package com.tx.doodle.api.service;

import com.tx.doodle.api.dao.PollRepository;
import com.tx.doodle.api.dao.UserRepository;
import com.tx.doodle.api.model.Initiator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.Mock;
import org.springframework.data.domain.PageRequest;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class PollServiceTest {

    @Mock
    UserRepository userRepository;

    @Mock
    PollRepository pollRepository;

    PollService victim;

    public static final String EMAIL = "test@doodle.com";
    public static final Initiator INITIATOR = new Initiator();
    public static final LocalDate FROM_DATE = LocalDate.now();
    public static final Timestamp FROM_DATE_TS = Timestamp.valueOf(FROM_DATE.atStartOfDay());
    public static final PageRequest PAGEABLE = PageRequest.of(0, 50);

    @BeforeEach
    public void setUp() {
        initMocks(this);
        victim = spy(new PollService(pollRepository, userRepository));
    }

    @Test
    void filterPolls() {
        doReturn(Optional.of(INITIATOR)).when(userRepository).findFirstByEmail(anyString());

        victim.filterPolls(EMAIL, FROM_DATE, PAGEABLE);

        verify(pollRepository).filterPolls(eq(INITIATOR), eq(FROM_DATE_TS), eq(PAGEABLE));
    }

    @Test
    void filterPollsWhenNoUserFound() {
        doReturn(Optional.empty()).when(userRepository).findFirstByEmail(EMAIL);

        Assertions.assertThrows(NoSuchElementException.class,
                () -> victim.filterPolls(EMAIL, FROM_DATE, PAGEABLE));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void filterPollsWithEmptyEmail(String email) {
        victim.filterPolls(email, FROM_DATE, PAGEABLE);

        verify(pollRepository).filterPolls(eq(null), eq(FROM_DATE_TS), eq(PAGEABLE));
    }

    @Test
    void filterPollsWithNullFromDate() {
        doReturn(Optional.of(INITIATOR)).when(userRepository).findFirstByEmail(EMAIL);

        victim.filterPolls(EMAIL, null, PAGEABLE);

        verify(pollRepository).filterPolls(eq(INITIATOR), eq(null), eq(PAGEABLE));
    }
}
